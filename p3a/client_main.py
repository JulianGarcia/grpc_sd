from client import Client
from client_stub import ClientStub
from time import time

HOST = 'localhost'
PORT = '50051'
#FILE = '/home/julian/sistemas-distribuidos/Práctica/practica1/file.txt'
FILE = '/home/julian/sistemas-distribuidos/asd.pdf'

def main():
    tiempo_inicial = time()
    stub = ClientStub(HOST, PORT)
    client = Client(HOST, PORT, stub)
    client.connect()
    #response = client.list_files('/home/julian')
    response = client.read_file(FILE)
    tiempo_final = time()
    tiempo_ejecucion = tiempo_final - tiempo_inicial
    print('Esta ejecucion tomo: ', tiempo_ejecucion)
    

if __name__ == "__main__":
    main()
    


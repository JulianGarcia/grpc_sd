import grpc
from concurrent import futures
import time

# generated classes
import file_system_pb2
import file_system_pb2_grpc
import file_system

class ClientStub:

  def __init__(self, host='0.0.0.0', port='50051'):
    self.url = ':'.join([host, port])
    self.channel = None
    self.stub = None

  def connect(self, host, port):    
    try:
      print(self.url)
      self.channel = grpc.insecure_channel(self.url)
      self.stub = file_system_pb2_grpc.FSStub(self.channel)
      return True if self.channel else False
    except Exception as e:
      print('error when openning channel {e}')
      return False

  def is_connected(self):
    return self.channel

  def list_files(self, path):
    if self.is_connected():
      path = file_system_pb2.Path(value=path)
      response = self.stub.list_files(path)
      return response.values
    else:
      return None

  def open_file(self, path):
    if self.is_connected():
      path = file_system_pb2.Path(value=path)
      response = self.stub.open_file(path)
      return response.value
    else:
      return None

  def close_file(self, path):
    if self.is_connected():
      path = file_system_pb2.Path(value=path)
      response = self.stub.close_file(path)
      return response.value
    else:
      return None

  def read_file(self, path, offset, number_bytes):
    if self.is_connected():
      path = file_system_pb2.ReadPath(value=path, offset=offset, number_bytes=number_bytes)
      response = self.stub.read_file(path)
      return response.read_value
    else:
      return None
import grpc
from concurrent import futures
import time

# generated classes
import file_system_pb2
import file_system_pb2_grpc

# original file_system.py
from file_system import FS


class StubFSServicer(file_system_pb2_grpc.FSServicer):

  def __init__(self, adapter):
    self._adapter = adapter
    file_system_pb2_grpc.FSServicer.__init__(self)

  def list_files(self, request, context):
    response = file_system_pb2.PathFiles()
    try:
      for file in FS.list_files(request.value):
        response.values.append(file)
    except Exception as e:
      print('ERROR -> -server- list files ', e)
    return response

  def open_file(self, request, context):
    response = file_system_pb2.Value()
    try:
      fd = FS.open_file(request.value)
      print(fd)
      response.value = fd
    except Exception as e:
      print('ERROR -> -server- open file ', e)
    return response

  def close_file(self, request, context):
    response = file_system_pb2.Value()
    try:
      response.value = FS.close_file(request.value)
    except Exception as e:
      print('ERROR -> -server- close file ', e)
    return response
    
  def read_file(self, request, context):
    response = file_system_pb2.ReadValue()
    print(response)
    print(request)
    try:
      _value = FS.read_file(request)
      response.read_value = _value
    except Exception as e:
      print('ERROR -> -server- read file ', e)
    return response

class ServerStub:
  def __init__(self, adapter, port='50051'):
    self._port = port
    self._adapter = adapter
    self.server = None

  def _setup(self):
    self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    file_system_pb2_grpc.add_FSServicer_to_server(StubFSServicer(self._adapter), self.server)
    print('Starting server. Listening on port 50051.')
    self.server.add_insecure_port('[::]:50051')
    
  def run(self):
    self._setup()
    self.server.start()
    try:
      while True:
        time.sleep(86400)
    except KeyboardInterrupt:
      self.server.stop(0)
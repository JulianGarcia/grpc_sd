import socket
import pickle
import file_system_structures
from concurrent import futures
import time

# original file_system.py
from file_system import FS


class ServerFSStub():

  def __init__(self, channel):
    self._channel = channel

  def _process_request(self):
    print('process request')
    pickle_data = self._channel.recv(4096)
    if pickle_data:
      path = pickle.loads(pickle_data)
      if path is not None:
        if path.operacion == 1:
          _path = path.value
          path_files = FS.list_files(_path)
          for _path in path_files:
            self._channel.sendall(_path)
        elif path.operacion == 2:
          path_read_value = FS.read_file(path)
          pickle_read_value = pickle.dumps(path_read_value)
          self._channel.sendall(pickle_read_value)
      return 0
    else:
      return 1

  
class ServerStub:
  def __init__(self, adapter, port=8090):
    self._port = port
    self._adapter = adapter
    self._stub = None
    self.server = None

  def _setup(self):
    self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(self._port)
    self.server.bind(("0.0.0.0", self._port)) 

  def run(self):
    self._setup()
    self.server.listen()
    try:
      while True:
        connection, client_address = self.server.accept()
        while True:
          from_client = ''
          self._stub = ServerFSStub(connection)
          if self._stub._process_request():
            break
    except KeyboardInterrupt:
      connection.close()
      self.server.stop(0)

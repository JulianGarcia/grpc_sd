# To run
- to run this need 2 terminals:
  
  * in the first terminal run: 
    > python3 server_main.py

  * after that, in the second terminal run:
    > python3 client_main.py

### Note:
 the file that the client is going to read is specified in *client_main.py* and te out file is specified in *client.py* 